package fr.uvsq.velizy.activetag;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import fr.uvsq.velizy.activetag.model.Historique;

import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;

public class MonAdapteur extends ArrayAdapter<Historique> {


    List<Historique> l;
    int r;
    Context c;

    public MonAdapteur(Context context, int resource, List<Historique> objects) {
        super(context, resource, objects);
        this.l = objects;
        this.r = resource;
        this.c = context;
    }


    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = ((Activity) c).getLayoutInflater();
        View v = inflater.inflate(r, parent, false);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm:ss", Locale.getDefault());


        Historique h = l.get(position);
        TextView tvhd = (TextView) v.findViewById(R.id.historiqueDateHeureDebut);
        tvhd.clearComposingText();
        tvhd.setText(simpleDateFormat.format(h.getDateDebut()));

        TextView tvhhd = (TextView) v.findViewById(R.id.historiqueDateHeureFin);
        tvhhd.clearComposingText();
        tvhhd.setText(simpleDateFormat.format(h.getDateFin()));

        TextView tvhe = (TextView) v.findViewById(R.id.historiqueActivité);
        tvhe.clearComposingText();
        tvhe.setText(h.getActivité());

        TextView tvhhf = (TextView) v.findViewById(R.id.historiqueContexte);
        tvhhf.clearComposingText();
        tvhhf.setText(h.getContexte());

        /*
        TextView tvhr = (TextView) v.findViewById(R.id.historiqueRemarque);
        tvhr.clearComposingText();
        tvhr.setText(h.getRemarque());
        */
        return v;
    }
}
