package fr.uvsq.velizy.activetag.model;

import java.util.Date;

public class Historique {

    private Date dateDebut;
    private Date dateFin;
    private String activité;
    private String contexte;
    private String remarque;

    public Historique(Date dateDebut, Date dateFin, String activité, String contexte, String remarque) {
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.activité = activité;
        this.contexte = contexte;
        this.remarque = remarque;
    }

    public String getContexte() {
        return contexte;
    }

    public void setContexte(String contexte) {
        this.contexte = contexte;
    }

    public String getActivité() {
        return activité;
    }

    public void setActivité(String activité) {
        this.activité = activité;
    }

    public Date getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    public Date getDateFin() {
        return dateFin;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    public String getRemarque() {
        return remarque;
    }

    public void setRemarque(String remarque) {
        this.remarque = remarque;
    }
}

