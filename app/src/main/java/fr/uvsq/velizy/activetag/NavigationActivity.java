package fr.uvsq.velizy.activetag;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;
import fr.uvsq.velizy.activetag.view.AccueilFragment;
import fr.uvsq.velizy.activetag.view.AnnotationFragment;
import fr.uvsq.velizy.activetag.view.HistoriqueFragment;

public class NavigationActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    private TextView mTextMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);

        Fragment fragment = new AccueilFragment();
        loadFragment(fragment);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(this);
    }

    private boolean loadFragment(Fragment fragment){
        if(fragment != null){

            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frangment_container, fragment)
                    .commit();

            return true;
        }
        return false;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        Fragment fragment = null;

        switch (item.getItemId()){
            case R.id.navigation_accueil:
                fragment = new AccueilFragment();
                break;

            case R.id.navigation_annotation:
                fragment = new AnnotationFragment();
                break;

            case R.id.navigation_historique:
                fragment = new HistoriqueFragment();
                break;

        }

        return loadFragment(fragment);
    }
}
