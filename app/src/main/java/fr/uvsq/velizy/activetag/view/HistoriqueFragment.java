package fr.uvsq.velizy.activetag.view;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import fr.uvsq.velizy.activetag.MonAdapteur;
import fr.uvsq.velizy.activetag.R;
import fr.uvsq.velizy.activetag.model.Historique;
import fr.uvsq.velizy.activetag.utils.CSVUtils;

import java.util.List;

public class HistoriqueFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_historique, null);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        List<Historique> list = CSVUtils.getCSVHistorique(this.getContext());
        //List<String> list = Arrays.asList("test 1","test 2");

        MonAdapteur adapter = new MonAdapteur(
                this.getContext(),
                R.layout.item,
                list);
        ListView lv = (ListView) this.getActivity().findViewById(R.id.lv);

        lv.setAdapter(adapter);
    }
}
