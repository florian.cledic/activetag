package fr.uvsq.velizy.activetag.utils;

import android.content.Context;
import android.util.Log;
import fr.uvsq.velizy.activetag.R;
import fr.uvsq.velizy.activetag.model.Historique;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.text.DateFormat;
import java.util.Locale;
import java.util.TimeZone;

public class CSVUtils {

    public static ArrayList<String[]> getCSVString(Context current){

        ArrayList<String[]> list = new ArrayList<>();

        InputStream is = current.getResources().openRawResource(R.raw.exemple_historique);

        BufferedReader reader = new BufferedReader(
                new InputStreamReader(is, Charset.forName("UTF-8"))
        );

        String line = "";
        try{
            while((line = reader.readLine()) != null){

                String tokens[] = line.split(";");

                list.add(tokens);

                //Faire ce que l'on veut des lignes


            }
            return list;
        }catch (IOException e){
            Log.wtf("MyActivity", "Erreur de lecture sur la ligne " + line, e);
            e.printStackTrace();
        }
        return list;
    }

    public static ArrayList<Historique> getCSVHistorique(Context current){

        ArrayList<Historique> list = new ArrayList<>();

        InputStream is = current.getResources().openRawResource(R.raw.exemple_historique);

        BufferedReader reader = new BufferedReader(
                new InputStreamReader(is, Charset.forName("UTF-8"))
        );

        String line = "";
        try{
            while((line = reader.readLine()) != null){

                String tokens[] = line.split(";");

                String sdd = tokens[0];
                String sdf = tokens[1];

                Historique historique;


                Date dateDebut = convertISO8601ToDate(sdd);
                Date dateFin = convertISO8601ToDate(sdf);

                switch(tokens.length){
                    case 5:
                        historique = new Historique(dateDebut, dateFin, tokens[2], tokens[3], tokens[4]);
                        list.add(historique);
                        break;

                    case 4:
                        historique = new Historique(dateDebut, dateFin, tokens[2], tokens[3], "");
                        list.add(historique);
                        break;
                }

                //Faire ce que l'on veut des lignes


            }
            return list;
        }catch (IOException e){
            Log.wtf("MyActivity", "Erreur de lecture sur la ligne " + line, e);
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return list;
    }

    private static Date convertISO8601ToDate(String s) throws ParseException {
        SimpleDateFormat dtf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.getDefault());
        return dtf.parse(s);
    }

}
