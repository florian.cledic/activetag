package fr.uvsq.velizy.activetag.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import fr.uvsq.velizy.activetag.R;
import fr.uvsq.velizy.activetag.utils.CSVUtils;

import java.util.ArrayList;
import java.util.Iterator;

public class MainActivity extends AppCompatActivity implements View .OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView textView = findViewById(R.id.activity_main_text);

        ArrayList<String[]> list = CSVUtils.getCSVString(this);

        Iterator i = list.iterator();

        while(i.hasNext()){
            for(String s : (String[]) i.next()){
                textView.append(s + " | ");
            }
            textView.append("\n");
        }

        Button b = findViewById(R.id.button_historique);
        b.setOnClickListener(this);



    }


    @Override
    public void onClick(View v) {
        startActivity(new Intent(this, HistoriqueFragment.class));
    }
}
